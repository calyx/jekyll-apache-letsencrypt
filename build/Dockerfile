FROM debian:bullseye

# exclude unnecessary files from being installed

RUN echo "path-exclude=/usr/share/locale/*\npath-exclude=/usr/share/man/*\npath-exclude=/usr/share/doc/*\npath-exclude=/usr/share/fonts/*" > /etc/dpkg/dpkg.cfg.d/nodoc

# packages needed to build gems

RUN apt-get update &&\
  apt-get install -y\
  apache2\
  build-essential\
  certbot\
  cron\
  git\
  inotify-tools\
  liblzma-dev\
  libssl-dev\
  python3-certbot-apache\
  ruby\
  ruby-dev\
  zlib1g-dev\
  &&\
  rm -rf /var/lib/apt/lists/* &&\
  rm -rf /usr/share/doc &&\
  rm -rf /usr/share/man &&\
  rm -rf /usr/share/fonts

# these gems are likely to be needed for jekyll

RUN gem install --no-document --bindir /usr/bin\
  bundler\
  ffi\
  i18n\
  jekyll\
  nokogiri\
  sassc\
  && rm -rf /usr/share/ri

# apache setup

RUN a2enmod ssl headers rewrite && a2dissite 000-default

# we will use /srv for websites and certs

VOLUME /srv
ENV SITES=""
EXPOSE 80
EXPOSE 443

# https://httpd.apache.org/docs/2.4/stopping.html#gracefulstop
# STOPSIGNAL SIGWINCH

COPY scripts /root/scripts
CMD ["ruby", "/root/scripts/start"]
