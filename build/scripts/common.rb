require 'yaml'
require 'erb'
require 'open3'

def get_bool_env(key, default=false)
  ENV[key].nil? ? default : ENV[key].to_s.downcase == "true"
end

def get_env(key, default=nil)
  ENV[key].nil? ? default : ENV[key].to_s.sub(/^"/, '').sub(/"$/, '')
end

def relative(path)
  File.expand_path("../" + path, __FILE__)
end

ENV_VARS = %w(
  SITES EMAIL
  SERVICE_DIR APACHE_DIR LE_DIR CERTBOT_DIR CERTBOT_ARGS
  BUILD_CMD ALLOW_HTTP DRYRUN DEBUG
)
SITES        = get_env "SITES"
EMAIL        = get_env "EMAIL"
SERVICE_DIR  = get_env "SERVICE_DIR", "/srv"
APACHE_DIR   = get_env "APACHE_DIR",  "/etc/apache2"
LE_DIR       = get_env "LE_DIR",      "/srv/letsencrypt"
CERTBOT_DIR  = get_env "CERTBOT_DIR", "/srv/certbot"
CERTBOT_ARGS = get_env "CERTBOT_ARGS"
BUILD_CMD    = get_env "BUILD_CMD"
DRYRUN       = get_bool_env "DRYRUN", false
DEBUG        = get_bool_env "DEBUG", false
CERTBOT      = get_bool_env "CERTBOT", false
ALLOW_HTTP   = get_bool_env "ALLOW_HTTP", !CERTBOT

APACHE_TEMPLATE_FILE  = relative("templates/apache-vhost.erb")
APACHE_DEFAULT_CONFIG = relative("templates/apache-default.conf")
APACHE_DEFAULT_INDEX  = relative("templates/index.html")
CRONTAB_FILE          = relative("templates/crontab")
APACHE_FOREGROUND     = relative("apache-foreground")
CERT_WATCHER          = relative("cert-watcher")
SITE_WATCHER          = relative("site-watcher")
APACHE_SITES_DIR      = File.join(APACHE_DIR, 'sites-enabled')

SUPPRESS_OUTPUT = [
  /might not work with crontab/,
  /installing your bundle as root will break this application for all non-root/,
  /users on this machine/,
  /Don't run Bundler as root/,
  /non-root users on this machine/,
  /to see where a bundled gem is installed/,
  /Auto-regeneration: disabled/,
  /Plugins selected:/
]

def setup
  system("mkdir", "-p", SERVICE_DIR)
  system("mkdir", "-p", APACHE_SITES_DIR)
  system("mkdir", "-p", LE_DIR)
  system("mkdir", "-p", CERTBOT_DIR)
  system("cp", APACHE_DEFAULT_INDEX, "/var/www/html")
  system("cp", APACHE_DEFAULT_CONFIG, File.join(APACHE_SITES_DIR, 'default.conf'))
end

def git_clone(domain, site, prefix: "GIT")
  Dir.chdir(SERVICE_DIR) do
    dest = File.join(SERVICE_DIR, domain)
    if File.exist?(dest)
      Dir.chdir(domain) do
        output "git reset --hard", prefix
        run("git", "reset", "--hard", prefix: prefix)
        output "git pull #{site['git']}", prefix
        run("git", "pull", prefix: prefix)
      end
    else
      output "git clone #{site['git']}", prefix
      run("git", "clone", site['git'], dest, prefix: prefix)
    end
  end
end

def bundle_install(domain, site, prefix: "BUNDLE")
  Dir.chdir(File.join(SERVICE_DIR, domain)) do
    if File.exist?("Gemfile.lock")
      run("rm", "Gemfile.lock")
    end
    # set bundler to use jobs equal to one less than the number of CPUs
    run("bundle config --global jobs $(expr $(grep -c processor /proc/cpuinfo) - 1)")
    run("mkdir", "-p", "/srv/gems")
    run("GEM_HOME=/srv/gems bundle", prefix: prefix)
  end
end

def jekyll_build(domain, site, prefix: "JEKYLL")
  Dir.chdir(File.join(SERVICE_DIR, domain)) do
    if BUILD_CMD
      output("Run BUILD_CMD: #{BUILD_CMD}")
      run(BUILD_CMD)
    end
    run("GEM_HOME=/srv/gems bundle exec jekyll build --destination ./static", prefix: prefix)
  end
end

def generate_apache_config(domain, site, prefix: nil)
  domain_aliases = site["domains"] - [domain]
  document_root = File.join(SERVICE_DIR, domain, "static")
  conf_str = apache_template().result(binding)
  conf_path = File.join(APACHE_SITES_DIR, "#{domain}.conf")
  File.open(conf_path, 'w') do |f|
    output "Write #{conf_path}", prefix
    f.write(conf_str)
  end
end

def apache_template
  template_str = File.read(APACHE_TEMPLATE_FILE)
  ERB.new(template_str, nil, '-')
end

def renew_cert(name, site, prefix: "CERBOT")
  output "renew #{name}", prefix
  cmd = [
    "certbot",
    "certonly",
    "--webroot",
    "--agree-tos",
    "--non-interactive",
    "--webroot-path", CERTBOT_DIR,
    "--logs-dir", File.join(CERTBOT_DIR, 'logs'),
    "--config-dir", LE_DIR,
    "-m", EMAIL
  ]
  if DRYRUN
    # --dry-run does not save cert
    # --test-cert does create a cert
    # it would be better to use --test-cert, but I can't
    # figure out a clean way to then remove these test certs
    # when you want to create a real one.
    cmd << "--dry-run"
  end
  if CERTBOT_ARGS
    cmd.concat(CERTBOT_ARGS.split(' '))
  end
  cmd << "--cert-name"
  cmd << site["domains"].first
  site["domains"].each do |domain|
    cmd << "-d"
    cmd << domain
  end
  run(*cmd, prefix: prefix)
end

def parse_env(verbose: false)
  if EMAIL.nil? || EMAIL == ""
    output "ERROR: environment variable EMAIL is not defined"
    exit
  end
  if SITES.nil? || SITES == ""
    output "ERROR: environment variable SITES is not defined"
    exit
  end
  sites = {}
  SITES.split(';').each do |site|
    domains, git_repo_url = site.split('=').map(&:strip)
    name = domains.split(',').first.strip
    sites[name] = {
      "git" => git_repo_url,
      "domains" => domains.split(',').map(&:strip)
    }
  end
  if verbose
    output "SETUP: environment variable SITES parses as:"
    sites.to_yaml.split("\n").each do |line|
      output line, "SETUP"
    end
  end
  return sites
rescue StandardError
  output "Could not parse environment variable SITES", "SETUP"
  output "SITES should be in the form:", "SETUP"
  output "DOMAIN_A_1,DOMAIN_A_2=GIT_REPO_URL_A;DOMAIN_B_1=GIT_REPO_URL_B;...", "SETUP"
  exit
end

#
# we need to inject the ENV somehow into the cron tasks.
# we can write these to a file that is sourced by the cron tasks,
# or we can prepend the variables to the crontab. for now, we use
# the latter approach.
#
def generate_crontab
  new_tab = env_var_array.join("\n") + "\n" + File.read(CRONTAB_FILE)
  File.open('/tmp/crontab', 'w') do |file|
    file.write(new_tab)
  end
  return '/tmp/crontab'
end

def env_var_array
  ENV_VARS.map {|varname|
    if ENV[varname]
      if ENV[varname].strip =~ /^".*"$/
        '%s=%s' % [varname, ENV[varname]]
      else
        '%s="%s"' % [varname, ENV[varname]]
      end
    end
  }.compact
end

def output(str, prefix=nil)
  if prefix
    STDOUT.puts "#{prefix}: #{str}"
  else
    STDOUT.puts str
  end
  STDOUT.flush
end

def run(*cmd)
  if cmd.last.is_a?(Hash)
    options = cmd.pop
  else
    options = {}
  end
  exit_status = -1
  if DEBUG
    output "RUN %s" % cmd.join(' ')
  end
  Open3.popen2e(ENV, *cmd) do |stdin, out, thread|
    while line = out.gets do
      unless !!SUPPRESS_OUTPUT.detect {|re| re.match line}
        output line, options[:prefix]
      end
    end
    exit_status = thread.value.exitstatus.to_i
  end
  if exit_status != 0
    output "ERROR", options[:prefix]
    exit exit_status
  end
end
