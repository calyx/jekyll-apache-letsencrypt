#!/bin/sh

docker run -p 80:80 -p 443:443 -v srv:/srv \
  -e CERTBOT \
  -e DRYRUN \
  -e SITES \
  -e EMAIL \
  -e BUILD_CMD \
  -it static
