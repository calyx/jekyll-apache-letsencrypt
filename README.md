A docker container to build & deploy multiple static Jekyll sites from git using Apache and Let's Encrypt.

Features:

* 100% configured from environment variables.
* Support as many domains and as many jekyll sites as you want.
* Just specify domains and git repository, and everything else is taken care of.
* Automatic Let's Encrypt certificates and renewals.
* Redirect http to https.
* Redirect www.domain to domain.
* Poll for git updates every 5 minutes.

Limitations:

* Only works with public git repositories.
* The shared gem cache `/srv/gems` might fill up with unwanted gems if the same volume is used over a long period of time.
* This produces a large docker image, because it contains everything needed to install and compile the gems used to build jekyll sites.
  * base debian: 100 MB
  * install apache, ruby, certbot: 400 MB
  * install default gems: 200 MB
* This violates the rule that a container should do just one thing, because certbot and jekyll are included with serving the web pages, but doing it this way keeps everything simple and self-contained.

Prequisites
===============================

Install docker

    # apt install docker.io
    # usermod -aG docker $USER
    $ newgrp docker

Make sure dockerd is running, start if not:

    # systemctl status docker
    # systemctl start docker

Run hello world:

    $ docker run hello-world

You probably don't want to run docker on boot:

    # systemctl disable docker

Build image
===============================

    $ cd build
    $ docker build -t static .

From time to time the debian base image will update to reflect new packages. To force an update that incorporates these changes to the base debian image:

    $ docker build --no-cache -t static .

Run container
===============================

Command switches:

* `-p 80:80`    -- accept HTTP on port 80
* `-p 443:443`  -- accept HTTPS on port 443
* `-v srv:/srv` -- allow persistant storage at /srv (required)
* `-e CERTBOT`  -- enable TLS certs from Let's Encrypt
* `-e SITES`    -- define sites to host (required)
* `-e EMAIL`    -- define Let's Encrypt email (required)
* `-n static`   -- optional name
* `-it`         -- optional, useful for testing, so you can hit ^C to
                   kill the container.

For example:

    $ docker run -p 80:80 -p 443:443 -v srv:/srv -e SITES="calyxos.org=https://gitlab.com/CalyxOs/calyxos.org.git" -e EMAIL="root@calyxos.org" -it static

Docker-compose
===============================

Alternately, you can let `docker-compose` handle the building and running.

Create `.env` with SITES and EMAIL:

    SITES="calyxos.org=https://gitlab.com/CalyxOs/calyxos.org.git"
    EMAIL="root@calyxos.org"
    CERTBOT=true

Start using `docker-compose`:

    docker-compose up

Update with `docker-compose`:

    docker-compose build --no-cache

Start with bash:

    docker-compose run static /bin/bash

Configuration
===============================

SITES
-------------------------------

The environent variable `SITES` is used to build the static jekyll pages and apache configuration.

The simple form:

    -e SITES="example.org=https://github.com/example/example.git"

The domain part can include multiple domains, separated by commas.

    -e SITES="example.org,www.example.org=https://github.com/example/example.git"

You can append multiple sites separated by a semicolon:

    -e SITES="site1.org=https://site1.git.url;site2.org=https://site2.git.url"

Other Variables
-------------------------------

* `CERTBOT`:      if set to "true" then it will attempt to get a valid TLS certificate
                  from Let's Encrypt. Default is "false".

* `ALLOW_HTTP`:   if set to "true" then apache will not force redirect all plaintext
                  http to https (default is whatever the opposite of CERTBOT is).

* `DRYRUN`:       if set to "true", then certbot will run in dryrun mode
                  (default "false").

* `CERTBOT_ARGS`: added to `certbot` command.
                  For example, `-e CERTBOT_ARGS="--force-renewal"`

* `BUILD_CMD`:    if set, run this command in the git directory before
                  running `jekyll build`.

TODO
===============================

Some things as non-root?

    RUN groupadd static && \
        useradd --create-home --no-log-init --gid static static
    RUN chown static:static /srv
    RUN chown static:static -R /home/static/scripts
    USER static

See also
===============================

* https://hub.docker.com/_/debian/
* https://jekyllrb.com/
* https://docs.docker.com/engine/reference/builder/
